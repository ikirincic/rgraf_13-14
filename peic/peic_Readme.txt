Opis

Za učitati geometrijsku datoteku cow.g koristila sam se načinom kako je korišteno na laboratorijskim vježbama kada smo 
radili s istim tim objektom: 'vtkBYUReader' te 'SetGeometryFileName' te bi prije pokretanja programa trebalo unijeti 
path za cow.g datoteku kako bi program radio bez grešaka.

Korišteni linkovi:
- http://www.vtk.org/
- http://www.mudri.uniri.hr (primjeri s laboratorijskih vježbi, pretežno s 2. gdje smo radili s istim ovim objektom)


