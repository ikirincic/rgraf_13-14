#include <iostream>
#include "vtkSmartPointer.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkLine.h"
#include "vtkLineSource.h"
#include "vtkAxesActor.h"
#include "vtkTransform.h"
#include "vtkBYUReader.h"
#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkOrientationMarkerWidget.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkPolyData.h"
#include "vtkPropAssembly.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkCamera.h"

//#include "pathconfig.h"


#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

int main()
{

  //ucitavanje geometrijske datoteke pomocu vtkBYUReader, dodjeljivanje naziva
  VTK_CREATE(vtkBYUReader, cow);
  cow->SetGeometryFileName("../src/cow.g");
  
  //pokusaj ucitavanja datoteke preko pathconfig - program se builda, ali ne pojavljuje se objekt nakon pokretanja
  //std::string str;
  //const char * path;
  //filePathBuilder fpb;

  //VTK_CREATE(vtkBYUReader, cow);
  //str=fpb.filePath("cow.g");
  //path=str.c_str();
  //cow->SetGeometryFileName(path);

  VTK_CREATE(vtkPolyDataMapper, mapper);
  mapper->SetInputConnection(cow->GetOutputPort());

  vtkSmartPointer<vtkTransform> cowtransform[3];
  vtkSmartPointer<vtkTransformPolyDataFilter> filter[3]; //filter za transformaciju koordinata točaka, potrebna su nam 3
  vtkSmartPointer<vtkActor> actor[3];

  //vtkTransform - za transformacije objekta
  cowtransform[0] = vtkSmartPointer<vtkTransform>::New();
  cowtransform[0]->Scale(-1, 1, 1);
  cowtransform[0]->Translate(-15, 0, 0);  //Translate(x,y,z)
  filter[0] = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
  filter[0]->SetTransform(cowtransform[0]);
  filter[0]->SetInputConnection(cow->GetOutputPort());
  filter[0]->Update();

  cowtransform[1] = vtkSmartPointer<vtkTransform>::New();
  cowtransform[1]->Scale(1, -1, 1);
  cowtransform[1]->Translate(0, -15, 0); //Translate(x,y,z)
  filter[1] = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
  filter[1]->SetTransform(cowtransform[1]);
  filter[1]->SetInputConnection(cow->GetOutputPort());
  filter[1]->Update();

  cowtransform[2] = vtkSmartPointer<vtkTransform>::New();
  cowtransform[2]->Scale(1, 1, -1);
  cowtransform[2]->Translate(0, 0, 10); //Translate(x,y,z)
  filter[2] = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
  filter[2]->SetTransform(cowtransform[2]);
  filter[2]->SetInputConnection(cow->GetOutputPort());
  filter[2]->Update();

  VTK_CREATE(vtkActor, cowActor_original);
  cowActor_original->SetMapper(mapper);
  cowActor_original->GetProperty()->SetColor(1.0,0,0); //original model prikazan u crvenoj boji
  cowActor_original->GetProperty()->SetRepresentationToWireframe();

  actor[0]=vtkSmartPointer<vtkActor>::New();
  actor[0]->SetMapper(mapper);
  actor[0]->SetUserTransform(cowtransform[0]);

  actor[1]=vtkSmartPointer<vtkActor>::New();
  actor[1]->SetMapper(mapper);
  actor[1]->SetUserTransform(cowtransform[1]);

  actor[2]=vtkSmartPointer<vtkActor>::New();
  actor[2]->SetMapper(mapper);
  actor[2]->SetUserTransform(cowtransform[2]);

  //vtkAxesActor - zastupanje 3D osi u sceni
  VTK_CREATE(vtkAxesActor, axes);
  VTK_CREATE(vtkAxesActor, cow2axes);
  cow2axes->AxisLabelsOff(); //oznake osi off


  VTK_CREATE(vtkRenderer, ren);
  ren->AddActor(cowActor_original);
  ren->AddActor(cow2axes);

  vtkCamera*camera = ren->GetActiveCamera();
  //namjestanje opcija kamere
  camera->SetPosition(0,0,30);
  camera->SetFocalPoint(0,0,0);
  camera->SetViewUp(0,1,0);
  camera->SetViewShear(1,1,1);

  //dodavanje objekata na renderer
  ren->AddActor(actor[0]);
  ren->AddActor(actor[1]);
  ren->AddActor(actor[2]);

  ren->SetBackground(1,1,1);  //postavljena bijela pozadina

  VTK_CREATE(vtkRenderWindow, renw);
  renw->AddRenderer(ren);

  //omogucuje upravljanje kamerom
  VTK_CREATE(vtkInteractorStyleTrackballCamera, style);

  VTK_CREATE(vtkRenderWindowInteractor, iren);
  iren->SetRenderWindow(renw);
  iren->SetInteractorStyle(style);
  iren->Initialize();
  iren->Start();


  return 0;
}

