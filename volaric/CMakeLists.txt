cmake_minimum_required(VERSION 2.6)
project(zadaca2)

find_package(GLUT)
find_package(OpenGL)

INCLUDE_DIRECTORIES(${GLUT_INCLUDE_DIR} ${OPENGL_INCLUDE_DIR} 
		    ${PROJECT_SOURCE_DIR})

set(CORELIBS ${GLUT_LIBRARY} ${OPENGL_LIBRARY})

add_executable(vedranvolaric vedranvolaric.cpp)
target_link_libraries(vedranvolaric ${GLUT_LIBRARY} ${OPENGL_LIBRARY})
