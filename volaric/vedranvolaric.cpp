/*
 * Zadatak: Kreriati četvrtinu plašta krnjeg konusa u OpenGL-u koristeći linije. Broj točaka za svaki z mora biti isti.
 * CMakeLists.txt
 *  add_executable(vedranvolaric vedranvolaric.cpp)
 *  target_link_libraries(vedranvolaric ${GLUT_LIBRARY} ${OPENGL_LIBRARY})
 */

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>

// Struktura koja opisuje jednu liniju plašta
struct Line
{
	float x1, z1, x2, z2;
};

// Linije plašta
int    g_linesSize;
Line * g_lines;

// Rotacija objekta
float g_rotateH;
float g_rotateV;

// Za interakciju mišem
bool g_rotating;
int  g_rotateLastX;
int  g_rotateLastY;

void init(void)
{
	g_rotateH = 0.0f;
	g_rotateV = 0.0f;

	g_rotating = false;
}

// Funkcija računa koordinate točaka linija tako da čine plašt stošca
void computeLines(Line * lines, int number_of_lines, float angle, float step, float radius1, float radius2)
{
	for (int line = 0; line < number_of_lines; line++)
	{
		float s = sinf(angle);
		float c = cosf(angle);

		lines[line].x1 = radius1 * s;
		lines[line].z1 = radius1 * c;
		lines[line].x2 = radius2 * s;
		lines[line].z2 = radius2 * c;

		angle += step;
	}
}

void createLines(int numberOfLines, float radius1, float radius2)
{
	g_lines     = new Line [numberOfLines];  // Alokacija memorije na C++ način
	g_linesSize = numberOfLines;

	float step = M_PI / (2 * numberOfLines);

	computeLines(g_lines, numberOfLines, 0.0, step, radius1, radius2);
}

void deleteLines()
{
	delete g_lines;  // Oslobađanje memorije na C++ način
}

// Funkcija iscrtava plašt koristeći unaprijed izračunate koordinate točaka
void drawLines(Line * lines, int n)
{
	for (int i = 0; i < n; i++)
	{
		glVertex3f(lines[i].x1, +1.0f, lines[i].z1);
		glVertex3f(lines[i].x2, -1.0f, lines[i].z2);
	}
}

// Funkcija koju GLUT poziva kada je potrebno (ponovno) iscrtati prozor
void render(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	// Enable transparency
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Perform view rotation
	glRotatef(g_rotateH, 0.0f, 1.0f, 0.0f);
	glRotatef(g_rotateV, 1.0f, 0.0f, 0.0f);

	glBegin(GL_LINES);
	{
		// Draw y axis
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);  // 0% transparent white
		glVertex3f(0.0f, -1.7f, 0.0f);
		glVertex3f(0.0f, +1.7f, 0.0f);

		glColor4f(1.0f, 1.0f, 0.0f, 0.5f);  // 50% transparent yellow
		drawLines(g_lines, g_linesSize);
	}
	glEnd();

	glFlush();
}

// Funkcija koju GLUT poziva kada se veličina prozora promijenila
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, (double) w / (double) h, 1.0, 1000.0);
	gluLookAt(-5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

void mouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			g_rotating = true;
			g_rotateLastX = x;
			g_rotateLastY = y;
		}
		else
		{
			g_rotating = false;
		}
	}
}

void mousemotion(int x, int y)
{
	if (g_rotating)
	{
		// Računaj pomak (deltu) i pridodaj vrijednostima kuteva rotacije
		g_rotateH += x - g_rotateLastX;
		g_rotateV += y - g_rotateLastY;

		// Spremi trenutne vrijednosti koordinata kao prethodne
		g_rotateLastX = x;
		g_rotateLastY = y;

		// Zatraži ponovno iscrtavanje
		glutPostRedisplay();
	}
}

int main(int argc, char ** argv)
{
	init();

	createLines(500, 0.25, 1.25);

	glutInit(&argc, argv);
	glutCreateWindow("OpenGL Window");
	glutReshapeFunc(resize);
	glutDisplayFunc(render);
	glutMouseFunc(mouse);
	glutMotionFunc(mousemotion);
	glutMainLoop();

	deleteLines();

	return 0;
}
