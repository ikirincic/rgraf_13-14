Opis:
--------------
Tekst zadatka:
Kreirati program koji će učitati tzv. height map te kreirati triangulirani teren, kreirate paletu boja
za prikaz, koristiti sva tri modela osvjetljenja te na mjestima gdje su gradijenti terena mali brisati
nepotrebne točke. Koristiti vtk biblioteku. Za primjer učitati Heightmap.png datoteku.

1. prvo sam pomoću vtkPng readera učitao sliku Heightmap.png koja mi je zadana u tekstu zadatka

2. nakon toga sam trebao napisati funkciju koja iz grayscale slike stvara niz točaka. Sa tim dijelom zadatka sam imao najviše problema
   i za njega sam morao tražiti pomoć interneta kao i nekih kolega.

3. sa objektom klase vtkPolyData koji kreira poligon(sliku) i u cijelinu stavlja točke sa skalarima(sa visinom)

4. za kreiranje trianguliranog terena sam koristio objekt klase vtkDelaunay2D

5. visine na slici, odnosno skalare preslikavam u boje pomoću vtkLookupTable, određujem ton boje skalarima(hue).

6. kreiranu mrežu trokuta u vtkDelaunay2D šaljem u vtkPolyDataMapper kao i vrijednosti skalara pretvorenih u boje.

7. nakon toga sam kreirao skalu za prikaz visine točaka. Boje na skali su iste kao i na slici, najniže visine su označene
   plavom bojom, a najviše točke su označene crvenom bojom. 

8. izlaz, odnosno podatke, iz vtkPolyDataMappera šaljem vtkActoru

9. nakon toga rendereru dajem 2 actora, jedan za renderiranje skale, a drugi za renderiranje slike(izlaz iz vtkActor-a)

10. dobiveni renderer dajem vtkRendererWindowu gdje namještam veličinu i poziciju prozora na kojem će se prikazati slika i 
    skala za prikaz visine

11. sa objektom klase vtkRendererWindowInteractor pokrećem renderer window.

Student: Nenad Vrkić
