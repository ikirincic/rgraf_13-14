// vrkic.cpp


#include "pathconfig.h"
#include <vtkPNGReader.h>
#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkLookupTable.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkActor.h>
#include <vtkScalarBarActor.h>
#include <vtkImageData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkDelaunay2D.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>

#define VTK_CREATE(type, name)  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

void Img_Points(vtkImageData * pImg, vtkPoints * pPoints, vtkFloatArray * pHeight);



int main(void)
{
    filePathBuilder fpath;
    //učitavanje slike Heightmap.png pomoću vtkPNGReadera
    VTK_CREATE(vtkPNGReader, pngreader);
    pngreader->SetFileName(fpath.filePath("Heightmap.png"));
    pngreader->Update();

    //kreiranje tocaka
    VTK_CREATE(vtkPoints, points);
	VTK_CREATE(vtkFloatArray, heights);
    Img_Points(pngreader->GetOutput(), points, heights);

    //kreiranje poligona
    VTK_CREATE(vtkPolyData, poly);
    poly->SetPoints(points);
    poly->GetPointData()->SetScalars(heights);

    //kreiranje trianguliranog terena
    VTK_CREATE(vtkDelaunay2D, del2D);
    del2D->SetInput(poly);

    //preslikavanje vrijednosti skalara u boje
	VTK_CREATE(vtkLookupTable, lut);
    lut->SetSaturationRange(1, 1);
    lut->SetTableRange(0.0, 0.0);
    lut->SetValueRange(1, 1);
    lut->SetHueRange(0.7, 0.0);

    //davanje polydata -> polyDataMapperu, mapira informacije o poligonima
    VTK_CREATE(vtkPolyDataMapper, mapper);
    mapper->SetInputConnection(del2D->GetOutputPort());
    mapper->ScalarVisibilityOn();
	mapper->SetLookupTable(lut);

    //kreiranje skale za prikaz visine u istoj boji kao i Heightmap.png
    VTK_CREATE(vtkScalarBarActor, scale);
    scale->SetPosition(0.05, 0.15);
    scale->SetOrientationToVertical();
    scale->SetLookupTable(lut);
    scale->SetHeight(0.5);


    //kreiranje actora i davanje mappera actoru
	VTK_CREATE(vtkActor, actor);
	actor->SetMapper(mapper);

    //davanje actora rendereru, i klasican tijek programa do samog prikaza na ekranu
    VTK_CREATE(vtkRenderer, ren);
    ren->AddActor(actor);
    ren->AddActor(scale);
    ren->SetBackground(0.0, 0.0, 0.0);


    VTK_CREATE(vtkRenderWindow, renw);
    renw->AddRenderer(ren);
    renw->SetSize(800,600);
    renw->SetPosition(300, 50);

    VTK_CREATE(vtkRenderWindowInteractor, iren);
    iren->SetRenderWindow(renw);
    iren->Start();

	return 0;
}

//funkcija koja iz date slike stvara niz točaka. Ovaj dio zadatka sam riješio uz pomoć vtk wiki tutorials
void Img_Points(vtkImageData * pImg, vtkPoints * pPoints, vtkFloatArray * pHeight){
    int extent [6];
    pImg->GetExtent(extent);

    for (int y = extent[2]; y < extent[3]; y++){

        for (int x = extent[0]; x < extent[1]; x++){
            double value = pImg->GetScalarComponentAsDouble(x, y, 0, 0);
            pPoints->InsertNextPoint(x, y, value / 4.0);
            pHeight->InsertNextValue(value / 255.0);
        }
    }
}

