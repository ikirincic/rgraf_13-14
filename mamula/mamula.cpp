#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkCutter.h>
#include <vtkPlane.h>
#include <vtkDataSetReader.h>
#include <vtkDataSetMapper.h>
#include <vtkOutlineFilter.h>
#include <vtkPointData.h>
#include <vtkCastToConcrete.h>
#include <vtkRectilinearGridGeometryFilter.h>
#include <vtkProperty.h>
#include "pathconfig.h"

#define VTK_CREATE(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New();

int main(){

  filePathBuilder fpb;

  //Učitati datoteku RectGrid2.vtk sa vtkDataSetReader objekta
  VTK_CREATE(vtkDataSetReader,Reader);
  Reader->SetFileName(fpb.filePath("RectGrid2.vtk").c_str());

  //Uspostaviti pipeline koji će vtkDataSetReader preko vtkCastToConcrete
  //objekta proizvesti vtkRectilinearGridGeometryFilter objekt
  VTK_CREATE (vtkCastToConcrete, cast);
  cast->SetInputConnection(Reader->GetOutputPort());

  VTK_CREATE (vtkRectilinearGridGeometryFilter, Filter);
  Filter->SetInputConnection(cast->GetOutputPort());

  //Kreirati tri međusobno okomite ravnine pomoću vtkPlane objekta
  VTK_CREATE(vtkPlane, plane1);
  plane1->SetOrigin(0,0,0.5);
  plane1->SetNormal(1,0,0);

  VTK_CREATE(vtkPlane, plane2);
  plane2->SetOrigin(0,0,0.5);
  plane2->SetNormal(0,1,0);

  VTK_CREATE(vtkPlane, plane3);
  plane3->SetOrigin(0,0,0.5);
  plane3->SetNormal(0,0,1);


  //definirati cut funkcije sa vtkCutter objektom
  VTK_CREATE(vtkCutter, cutter1);
  cutter1->SetCutFunction(plane1);
  cutter1->SetInputConnection(Reader->GetOutputPort());
  cutter1->Update();

  VTK_CREATE(vtkCutter, cutter2);
  cutter2->SetCutFunction(plane2);
  cutter2->SetInputConnection(Reader->GetOutputPort());
  cutter2->Update();

  VTK_CREATE(vtkCutter, cutter3);
  cutter3->SetCutFunction(plane3);
  cutter3->SetInputConnection(Reader->GetOutputPort());
  cutter3->Update();


  //Outline objekta prikazati pomoću vtkOutlineFilter objekta
  VTK_CREATE(vtkOutlineFilter,outlineFilter);
  outlineFilter-> SetInputConnection(Reader->GetOutputPort());


  //stvaranje mappera za cuttere i za outline
  VTK_CREATE(vtkDataSetMapper, mapper1);
  mapper1->SetInputConnection(cutter1->GetOutputPort());
  mapper1->SetScalarRange(Reader->GetOutput()->GetPointData()->GetScalars()->GetRange());

  VTK_CREATE(vtkDataSetMapper, mapper2);
  mapper2->SetInputConnection(cutter2->GetOutputPort());
  mapper2->SetScalarRange(Reader->GetOutput()->GetPointData()->GetScalars()->GetRange());

  VTK_CREATE(vtkDataSetMapper, mapper3);
  mapper3->SetInputConnection(cutter3->GetOutputPort());
  mapper3->SetScalarRange(Reader->GetOutput()->GetPointData()->GetScalars()->GetRange());

  VTK_CREATE(vtkDataSetMapper, outlineMapper);
  outlineMapper-> SetInputConnection(outlineFilter->GetOutputPort());



  //actors
  VTK_CREATE(vtkActor,actor1);
  actor1-> SetMapper(mapper1);

  VTK_CREATE(vtkActor,actor2);
  actor2-> SetMapper(mapper2);

  VTK_CREATE(vtkActor,actor3);
  actor3-> SetMapper(mapper3);

  VTK_CREATE(vtkActor,outlineActor);
  outlineActor-> SetMapper(outlineMapper);
  outlineActor->GetProperty()->SetColor(1,1,1);//Outline objekt - bijela boja


  //renderer
  VTK_CREATE(vtkRenderer,ren);
  ren-> SetBackground(0.7,0.7,0.7);//pozadina - sivkasta nijansa
  ren->AddActor(actor1);
  ren->AddActor(actor2);
  ren->AddActor(actor3);
  ren->AddActor(outlineActor);

  //renderWindow
  VTK_CREATE(vtkRenderWindow,renw);
  renw->AddRenderer(ren);
  renw-> SetSize(400, 400);


  //interactor
  VTK_CREATE(vtkRenderWindowInteractor,iren);
  iren->SetRenderWindow(renw);
  iren->Initialize();
  iren->Start();


  return 0;
}
