#include "vtkSmartPointer.h"
#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkBYUReader.h"
#include "vtkAxesActor.h"
#include "vtkCamera.h"
#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkTransform.h"
#include "vtkTransformFilter.h"
#include "vtkLine.h"
#include "vtkLineSource.h"
#include "pathconfig.h"
#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()


int main(int argc, char *argv[])
{





int n, broj=0;
double t1[3]={0,0,0}, t2[3]={10,10,10}; //tocke za os rotacije

std::string str;
const char * path;

  if(!argv[1]){
    printf("Niste unjeli broj krava kao argument programa \n npr: ./batistic 7");
    exit(0);
    }
  sscanf(argv[1], "%d", &n);
  filePathBuilder builder;


  VTK_CREATE(vtkBYUReader, krava);
  //krava->SetGeometryFileName("../../rgraf_13-14/data/cow.g");
  str=builder.filePath("cow.g");
  path=str.c_str();
  krava->SetGeometryFileName(path);

  //linija
  VTK_CREATE(vtkLineSource, linesource);
  linesource->SetPoint1(t1);
  linesource->SetPoint2(t2);
  linesource->SetResolution(10);
  linesource->Update();

  VTK_CREATE(vtkPolyDataMapper, mapper2);
  mapper2->SetInputConnection(linesource->GetOutputPort());

  VTK_CREATE(vtkPolyDataMapper, mapper);
  mapper->SetInputConnection(krava->GetOutputPort());


  VTK_CREATE(vtkActor, krava_actor_original);
  krava_actor_original->SetMapper(mapper);
  krava_actor_original->GetProperty()->SetColor(0.96,0.87,0.7);
  krava_actor_original->GetProperty()->SetRepresentationToWireframe();

  vtkSmartPointer<vtkTransform> *trans= new vtkSmartPointer<vtkTransform>[n];
  vtkSmartPointer<vtkTransformFilter> *tf= new vtkSmartPointer<vtkTransformFilter>[n];
  vtkSmartPointer<vtkPolyDataMapper>  *tm= new vtkSmartPointer<vtkPolyDataMapper> [n];
  vtkSmartPointer<vtkActor> *ta= new vtkSmartPointer<vtkActor>[n];
  VTK_CREATE(vtkRenderer, ren);

for(int i=0;i<n;i++){

  trans[i]=vtkSmartPointer<vtkTransform>::New();
  trans[i]->RotateWXYZ(broj,t1[0]-t2[0],t1[1]-t2[1],t1[2]-t2[2]);
  trans[i]->RotateZ(-45);
  trans[i]->RotateX(30);
  trans[i]->Translate(0,0,10);
  broj+=360/n;



  tf[i]=vtkSmartPointer<vtkTransformFilter>::New();
  tf[i]->SetInputConnection(krava->GetOutputPort());
  tf[i]->SetTransform(trans[i]);

  tm[i]=vtkSmartPointer<vtkPolyDataMapper>::New();
  tm[i]->SetInputConnection(tf[i]->GetOutputPort());

  ta[i]=vtkSmartPointer<vtkActor>::New();
  ta[i]->SetMapper(tm[i]);
  ta[i]->GetProperty()->SetColor(0.1,0.87,0.7);
  ta[i]->GetProperty()->SetRepresentationToWireframe();



  ren->AddActor(ta[i]);


}


  VTK_CREATE(vtkActor, lineactor);
  lineactor->SetMapper(mapper2);
  lineactor->GetProperty()->SetLineWidth(2);

  VTK_CREATE(vtkAxesActor, cow2axes);
  ren->AddActor(cow2axes);
  ren->AddActor(lineactor);

  cow2axes->AxisLabelsOff();


  vtkCamera* camera = ren->GetActiveCamera();
  //postavljanje kamere
  camera->SetPosition(0,0,20);
  camera->SetFocalPoint(0,0,0);
  camera->SetViewUp(0,1,0);

  ren->SetBackground(.1,.2,.3);

  VTK_CREATE(vtkRenderWindow, renw);
  renw->AddRenderer(ren);

  VTK_CREATE(vtkInteractorStyleTrackballCamera, style);

  VTK_CREATE(vtkRenderWindowInteractor, iren);
  iren->SetRenderWindow(renw);
  iren->SetInteractorStyle(style);
  iren->Initialize();
  iren->Start();

  return 0;

  if (argc==1){}; //rjesavanje warninga za argc, bez ove linije dolazi warning da je argc neiskoristen
}
