#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

#define DEGREE_STEP 5.f
#define FULL_CIRCLE 360.f

GLdouble radius= 1.f;        //globalne varijable za spremanje argumenata i za korištenje u glut funkcija
GLdouble lightposX= 1.f;
GLdouble lightposY= 1.f;
GLdouble lightposZ= 1.f;

GLdouble rotX =10.f; //globalne varijable rotacije
GLdouble rotY =10.f;
GLdouble rotZ =10.f;

int lastPressed=0;

void init(void)
{
   glClearColor (0.0, 0.0, 0.0, 0.0);
   glShadeModel (GL_SMOOTH);
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);
   glEnable(GL_DEPTH_TEST);
}

/*  Here is where the light position is reset after the modeling
 *  transformation (glRotated) is called.  This places the
 *  light at a new position in world coordinates.  The cube
 *  represents the position of the light.
 */
void display(void)
{
   GLfloat position[] = { 0.0, 0.0, 1.5, 1.0 };

   glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   glPushMatrix ();
       glTranslatef (0.0, 0.0, -5.0);

       glPushMatrix ();
           glRotatef(rotX, 1.f, 0.f, 0.f);
           glRotatef(rotY, 0.f,  1.f, 0.f);
           glRotatef(rotZ, 0.f, 0.f, 1.f);
           glTranslated (0.0, 0.0, 1.5);
           glutSolidSphere( radius, 20, 10);
       glPopMatrix ();

       glLightfv (GL_LIGHT0, GL_POSITION, position);
       glDisable (GL_LIGHTING);
       glColor3f (0.0, 1.0, 1.0);
       glutWireCube (0.1);
       glEnable (GL_LIGHTING);
   glPopMatrix ();

   glFlush ();
}

void reshape (int w, int h)
{
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(40.0, (GLfloat) w/(GLfloat) h, 1.0, 20.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

void FKeys(int key, int x, int y)
{
    int mod;

    switch (key)
    {
        case GLUT_KEY_F1 :
            lastPressed=1;
            rotX += DEGREE_STEP;
            if (rotX >= FULL_CIRCLE)
                rotX = 0.;
            cout << "Rotacija sfere oko X, X=" << rotX << endl ;
            glutPostRedisplay();
        break;

        case GLUT_KEY_F2 :
            lastPressed=2;
            rotY += DEGREE_STEP;
            if (rotY > FULL_CIRCLE) rotY = 0.;
            cout << "Rotacija sfere oko Y, Y=" << rotY << endl ;
            glutPostRedisplay();
        break;

        case GLUT_KEY_F3 :
            lastPressed=3;
            rotZ += DEGREE_STEP;
            if (rotZ > FULL_CIRCLE) rotZ = 0.;
            cout << "Rotacija sfere oko Z, Z=" << rotZ << endl ;
            glutPostRedisplay();
        break;
    }
}


int main(int argc, char** argv)
{
    /*Riješavanje argumenata*/

        //radius
        if (!argv[1]) {
            cout << "Nije zadan radius ni XYZ svijetla. Default= 1.f.\n";
        } else {
            radius = atof(argv[1]);
           cout << "radius="<< radius <<endl;
            if (radius > 1.f || radius <= 0.f) radius = 1.f; // stavljanje na default ako ne ulazi u uvjete
        }

        //lightposX
        if (!argv[2]) {
            cout << "Nije zadan X, Y i Z svijetla. Default= 1.f.\n";
        } else {
            lightposX = atof(argv[2]);
            cout << "lightposX="<< lightposX <<endl;
            if (lightposX > 1.f || lightposX < -1.f) lightposX = 1.f; // stavljanje na default ako ne ulazi u uvjete
        }

        //lightposY
        if (!argv[3]) {
            cout << "Nije zadan Y i Z svijetla. Default= 1.f.\n";
        } else {
            lightposY = atof(argv[3]);
            cout << "lightposY="<< lightposY<<endl;
            if (lightposY > 1.f || lightposY < -1.f) lightposY = 1.f; // stavljanje na default ako ne ulazi u uvjete
        }

        //lightposZ
        if (!argv[4]) {
            cout << "Nije zadan Z svijetla. Default= 1.f.\n";
        } else {
            lightposZ = atof(argv[4]);
            cout << "lightposZ="<< lightposZ <<endl;
            if (lightposZ > 1.f || lightposZ < -1.f) lightposZ = 1.f; // stavljanje na default ako ne ulazi u uvjete
        }

        /*Iscrtavanje*/


   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize (500, 500);
   glutInitWindowPosition (100, 100);
   glutCreateWindow (argv[0]);
   init ();
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutSpecialFunc(FKeys);
   glutMainLoop();
   return 0;
}
