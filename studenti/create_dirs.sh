#! /bin/bash
dirovi=(balent batistic cvitkusic devcic dubrovic golubic grskovic  \
	gurguska hodanic jovic jumic jurada kirincic konjuh krizanovic \
	lackovic mamula mance mandic marmilic milakovic mileta nacinovic \
	papic paskvan pavkovic peic petrovic poropat poscic radman ruzic \
	smrekar spetic suljic tomas tomljanovic vidovic vinkovic volaric \
	vrankovic vrkic zaplotnik zgrablic)

function create_files(){
cat > $1_Readme.txt << EOF
Opis
EOF
cat > CMakeLists.txt << EOF
#
# OPEN_GL primjer
#
#add_executable(MirkoFodor MirkoFodor.cpp)
#target_link_libraries(MirkoFodor ${GLUT_LIBRARY} ${OPENGL_LIBRARY})

#
# VTK primjer
# 
#add_executable(ReneBitorajac ReneBitorajac.cpp)
#target_link_libraries(ReneBitorajac vtkRendering vtkWidgets vtkGraphics vtkHybrid vtkVolumeRendering)
EOF
cat > $1.cpp << EOF
#include <iostream>
int main(){return 0;}
EOF
}
DIR= 
for name in "${dirovi[@]}"
do
  mkdir $name
  cd $name
  create_files $name
  cd ../
done
