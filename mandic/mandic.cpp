#include <vtkSmartPointer.h>
#include "vtkInteractorStyleTrackballCamera.h"
#include <iostream>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkQuad.h>
#include <vtkCellArray.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <GL/glut.h>
#include "vtkTransform.h"
#include "vtkTransformFilter.h"


#define VTK_CREATE(type, name)vtkSmartPointer<type> name =
vtkSmartPointer<type>::New()

int main(int argc, char **argv)
{

  double p0[23] = {0,0,-1};
  double p1[23] = {2,0,-2};
  double p2[23] = {2,2,0};
  double p3[23] = {0,2,1};

  double p4[23] = {2,0,-2};
  double p5[23] = {4,0,1};
  double p6[23] = {4,2,-1};
  double p7[23] = {2,2,0};

  double p8[23] = {4,0,1};
  double p9[23] = {6,0,2};
  double p10[23] = {6,2,-2};
  double p11[23] = {4,2,-1};

  double p12[23] = {0,2,1};
  double p13[23] = {2,2,0};
  double p14[23] = {2,4,-0.6};
  double p15[23] = {0,4,2};

  double p16[23] = {2,2,0};
  double p17[23] = {4,2,-1};
  double p18[23] = {4,4,0.8};
  double p19[23] = {2,4,-0.6};

  double p20[23] = {4,2,-1};
  double p21[23] = {6,2,-2};
  double p22[23] = {6,4,0};
  double p23[23] = {4,4,0.8};


  VTK_CREATE(vtkPoints,points);
  points->InsertNextPoint (p0);
  points->InsertNextPoint (p1);
  points->InsertNextPoint (p2);
  points->InsertNextPoint (p3);

  points->InsertNextPoint (p4);
  points->InsertNextPoint (p5);
  points->InsertNextPoint (p6);
  points->InsertNextPoint (p7);

  points->InsertNextPoint (p8);
  points->InsertNextPoint (p9);
  points->InsertNextPoint (p10);
  points->InsertNextPoint (p11);

  points->InsertNextPoint (p12);
  points->InsertNextPoint (p13);
  points->InsertNextPoint (p14);
  points->InsertNextPoint (p15);

  points->InsertNextPoint (p16);
  points->InsertNextPoint (p17);
  points->InsertNextPoint (p18);
  points->InsertNextPoint (p19);

  points->InsertNextPoint (p20);
  points->InsertNextPoint (p21);
  points->InsertNextPoint (p22);
  points->InsertNextPoint (p23);

  VTK_CREATE(vtkQuad,quad);
  quad->GetPointIds()->SetId(0,0);
  quad->GetPointIds()->SetId(1,1);
  quad->GetPointIds()->SetId(2,2);
  quad->GetPointIds()->SetId(3,3);

  VTK_CREATE(vtkQuad,quad2);
  quad2->GetPointIds()->SetId(0,4);
  quad2->GetPointIds()->SetId(1,5);
  quad2->GetPointIds()->SetId(2,6);
  quad2->GetPointIds()->SetId(3,7);

  VTK_CREATE(vtkQuad,quad3);
  quad3->GetPointIds()->SetId(0,8);
  quad3->GetPointIds()->SetId(1,9);
  quad3->GetPointIds()->SetId(2,10);
  quad3->GetPointIds()->SetId(3,11);

  VTK_CREATE(vtkQuad,quad4);
  quad4->GetPointIds()->SetId(0,12);
  quad4->GetPointIds()->SetId(1,13);
  quad4->GetPointIds()->SetId(2,14);
  quad4->GetPointIds()->SetId(3,15);

  VTK_CREATE(vtkQuad,quad5);
  quad5->GetPointIds()->SetId(0,16);
  quad5->GetPointIds()->SetId(1,17);
  quad5->GetPointIds()->SetId(2,18);
  quad5->GetPointIds()->SetId(3,19);

  VTK_CREATE(vtkQuad,quad6);
  quad6->GetPointIds()->SetId(0,20);
  quad6->GetPointIds()->SetId(1,21);
  quad6->GetPointIds()->SetId(2,22);
  quad6->GetPointIds()->SetId(3,23);

  VTK_CREATE(vtkCellArray,quads);
  quads->InsertNextCell(quad);
  quads->InsertNextCell(quad2);
  quads->InsertNextCell(quad3);
  quads->InsertNextCell(quad4);
  quads->InsertNextCell(quad5);
  quads->InsertNextCell(quad6);

  VTK_CREATE(vtkTransform, trans1);
  VTK_CREATE(vtkTransformFilter, tf1);
  trans1->Scale(1,0,0);

  tf1->SetTransform(trans1);

  VTK_CREATE(vtkPolyData,polydata);

  polydata->SetPoints ( points );
  polydata->SetPolys ( quads );

  VTK_CREATE(vtkPolyDataMapper, mapper);
  mapper->SetInput(polydata);

  VTK_CREATE(vtkActor, actor);
  actor->SetMapper(mapper);
  actor->GetProperty()->SetColor(0,0.7,0);
  actor->GetProperty()->SetEdgeColor(0,0.7,0);
  actor->GetProperty()->EdgeVisibilityOn();
  actor->GetProperty()->SetAmbient(0.125);
  actor->GetProperty()->SetDiffuse(0.0);
  actor->GetProperty()->SetSpecular(0.0);
  actor->SetScale(1.0,1.0,2.0);

  VTK_CREATE(vtkRenderer, ren);
  VTK_CREATE(vtkRenderWindow, renWin);
  renWin->AddRenderer(ren);

  VTK_CREATE(vtkInteractorStyleTrackballCamera, style);
  VTK_CREATE(vtkRenderWindowInteractor, iren);
  iren->SetRenderWindow(renWin);
  iren->SetInteractorStyle(style);
  ren->AddActor(actor);
  ren->SetBackground(0.5,0.5,0.5);

  VTK_CREATE(vtkRenderWindow, renw);
  renw->AddRenderer(ren);
  renw->SetSize(500,500);
  iren->SetRenderWindow(renw);

  iren->Initialize();
  iren->Start();


  return 0;
}
