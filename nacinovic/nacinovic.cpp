/*  ZADATAK:
    Kreirati kvadar proizvoljnih dimenzija.Odgovarajućim tipkama na tipkovnici upravljati pozicijom kamere(očišta),
    njenom žarišnom točkom, ravninama rezanja, te rotacijom oko kvadra.
    Potrebno je upotrijebiti sva tri modela osvijetljenja i dodati jedan dodatni izvor svjetla.
*/

#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>

float _angle[3] = {0.0f,0.0f,0.0f};
float near = 1.0f;
float far = 20.0f;
float camera[3]={0.0,0.0,5.0}; //pozicija kamere
float focus[3]={0.0,0.0,0.0}; //pozicija fokusa
float a,b,c; // dimenzije kvadra

void drawScene();
void handleKeypress(unsigned char key, int x, int y );

int main(int argc, char **argv)
{
    glutInit(&argc, argv);     // inicijalizira GLUT
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // omogucava prikaz boja, depth buffer i double buffering (koristi se u animaciji/dinamickim programima)
    glutInitWindowSize(500, 500);   // zadamo velicinu prozora
    glutCreateWindow("Zadatak 23 - Kvadar");    // naslov prozora

    // dimenzije kvadra unosimo preko command line argumenata; definiramo defaultne vrijednosti ako nismo unijeli nista
    if (argc<3) {
        a=1.0f;
        b=1.2f;
        c=0.7f;
    } else {
        a=atof(argv[1]);
        b=atof(argv[2]);
        c=atof(argv[3]);
    }

    glutDisplayFunc(drawScene); // poziva se funkcija drawScene i to prvi put kad se prozor pojavi te svaki sljedeci put kad se generira repaint zahtjev
    glutKeyboardFunc(handleKeypress);

    glutMainLoop();
    return 0;
}

void rotate(int x, int y, int z)
{
    if (x) {
        _angle[0]+=3.0f*x;
        if (_angle[0] > 360) {
            _angle[0] -= 360;
        }
        if (_angle[0] < 0) {
            _angle[0] += 360;
        }
    } else if (y) {
        _angle[1]+=3.0f*y;
        if (_angle[1] > 360) {
            _angle[1] -= 360;
        }
        if (_angle[1] < 0){
            _angle[1] += 360;
        }
    } else if (z) {
        _angle[2]+=3.0f*z;
        if (_angle[2] > 360) {
            _angle[2] -= 360;
        }
        if (_angle[2] < 0) {
            _angle[2] += 360;
        }
    }

    glutPostRedisplay();    // repaint zahtjev, poziva se drawScene() funkcija
}

void ChangeNear(int temp)
{
   if ((temp<0 && near>1.0 ) || (temp>0 && near<far)) {
        near += temp*1.0f;
    }
    glutPostRedisplay();
}

void ChangeFar(int temp)
{
    if ((temp<0 && far>near ) || (temp>0)) {
        far += temp*1.0f;
    }
    glutPostRedisplay();
}

void MoveCamera(int x, int y, int z)
{
    if (x) {
        camera[0]+=x;
    } else if (y) {
        camera[1]+=y;
    } else if (z) {
        camera[2]+=z;
    }
    glutPostRedisplay();
}

void MoveFocus(int x, int y, int z)
{
    if (x) {
        focus[0]+=x;
    } else if (y) {
        focus[1]+=y;
    } else if (z) {
        focus[2]+=z;
    }
    glutPostRedisplay();
}

void resetView()
{

    _angle[0] = 0.0f;
    _angle[1] = 0.0f;
    _angle[2] = 0.0f;
    near = 1.0f;
    far = 20.0f;

    camera[0] = 0.0f;
    camera[1] = 0.0f;
    camera[2] = 5.0f;

    focus[0] = 0.0f;
    focus[1] = 0.0f;
    focus[2] = 0.0f;

    glutPostRedisplay();
}

void handleKeypress(unsigned char key, int x, int y )
{
    (void)(x);
    (void)(y);
    
    switch (key)
    {
        case 27:  // pritiskom na Escape tipku izlazimo iz programa
            exit(0);
            break;

        //rotacije oko osi
        case 'x':
            rotate(1,0,0);
            break;
        case 's':
            rotate(-1,0,0);
            break;
        case 'y':
            rotate(0,1,0);
            break;
        case 't':
            rotate(0,-1,0);
            break;
        case 'z':
            rotate(0,0,1);
            break;
        case 'a':
            rotate(0,0,-1);
            break;

        // micanje kamere
        case 'd':
            MoveCamera(1,0,0);
            break;
        case 'c':
            MoveCamera(-1,0,0);
            break;
        case 'v':
            MoveCamera(0,1,0);
            break;
        case 'b':
            MoveCamera(0,-1,0);
            break;
        case 'n':
            MoveCamera(0,0,1);
            break;
        case 'm':
            MoveCamera(0,0,-1);
            break;

        // pomicanje zarista
        case '6':
            MoveFocus(1,0,0);
            break;
        case '4':
            MoveFocus(-1,0,0);
            break;
        case '8':
            MoveFocus(0,1,0);
            break;
        case '2':
            MoveFocus(0,-1,0);
            break;
        case '1':
            MoveFocus(0,0,1);
            break;
        case '3':
            MoveFocus(0,0,-1);
            break;

        // ravnine rezanja
        case 'i':
            ChangeNear(-1);
            break;
        case 'j':
            ChangeNear(1);
            break;
        case 'k':
            ChangeFar(-1);
            break;
        case 'l':
            ChangeFar(1);
            break;

        // resetiraj sve, vrati na pocetno stanje
        case 'r':
            resetView();
            break;
    }
}

void drawScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,1.0,near,far);

    glMatrixMode(GL_MODELVIEW); //promijeni u drawing perspective
    glLoadIdentity(); //Resetiraj drawing perspective

    //definiramo poziciju kamere i poziciju zarista
    gluLookAt(camera[0], camera[1], camera[2],
              focus[0], focus[1], focus[2],
           0.0f,1.0f,0.0f);

    glRotatef(_angle[0], 1.0f, 0.0f, 0.0f);
    glRotatef(_angle[1], 0.0f, 1.0f, 0.0f);
    glRotatef(_angle[2], 0.0f, 0.0f, 1.0f);

    // ambijentna komponenta osvjetljenja
    GLfloat ambientLight[]={0.3f, 0.3f, 0.7f, 1};
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);

    // zrcalna komponenta osvjetljenja
    GLfloat specular[] = {0.5f, 0.2f, 0.7f, 1};
    //GLfloat specular[] = {0.3f, 0.3f, 0.7f, 1};
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

    // difuzna komponenta osvjetljenja
    GLfloat diffuse[] = {0.3f, 0.3f, 0.7f, 1};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);

    // position light - dodatni izvor svjetla
    GLfloat pos[] = {0.0f, 2.9f, 3.0f, 2.0f};
    glLightfv(GL_LIGHT0, GL_POSITION, pos);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // kvadar (plave boje - podeseno pomocu komponenata osvjetljenja)
    glBegin(GL_QUADS);
    glVertex3f(-a, -b, -c);
    glVertex3f(a, -b, -c);
    glVertex3f(a, b, -c);
    glVertex3f(-a, b, -c);

    glVertex3f(-a, b, -c);
    glVertex3f(a, b, -c);
    glVertex3f(a, b, c);
    glVertex3f(-a, b, c);

    glVertex3f(-a, -b, c);
    glVertex3f(a, -b, c);
    glVertex3f(a, b, c);
    glVertex3f(-a, b, c);

    glVertex3f(-a, -b, -c);
    glVertex3f(a, -b, -c);
    glVertex3f(a, -b, c);
    glVertex3f(-a, -b, c);

    glVertex3f(-a, -b, -c);
    glVertex3f(-a, b, -c);
    glVertex3f(-a, b, c);
    glVertex3f(-a, -b, c);

    glVertex3f(a, -b, -c);
    glVertex3f(a, b, -c);
    glVertex3f(a, b, c);
    glVertex3f(a, -b, c);
    glEnd();

    glutSwapBuffers();    // Zamjena front i back buffera (buduci da se sljedeci prikaz priprema u back bufferu, dok se trenutni prikaz cuva u front bufferu)

}

