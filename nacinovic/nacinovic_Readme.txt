Opis

Moj zadatak je bio napraviti kvadar proizvoljnih dimenzija te raditi razne manipulacije s njim. Dimenzije kvadra se unose putem command line args, s time da ukoliko programu nisu dani argumenti prilikom pozivanja, uzima defaultne vrijednosti (dimenzije bi trebale biti negdje izmedju 0 i 2 za optimalan rad programa zbog pozicije kamere).
Pritiskom na razlicita slova ostvaraju se drukcije funkcionalnosti. Probala sam nekako smisleno povezati slova s funkcijom:
  rotacija oko osi x: x,s
  rotacija oko osi y: y,t
  rotacija oko osi z: z,a

  micanje kamere : d,c,v,b,n,m

  pomicanje zarisne tocke gore: 8
  pomicanje zarisne tocke dolje: 2
  pomicanje zarisne tocke lijevo: 4
  pomicanje zarisne tocke desno: 6
  pomicanje zarisne tocke po z osi: 1, 3

  micanje near clipping plane naprijed: i
  micanje near clipping plane nazad: j
  micanje far clipping plane naprijed: k 
  micanje far clipping plane nazad: l

Pogled se resetira pritiskom na slovo 'r' - cime se sve vrijednosti vracaju na pocetno stanje.


Prilikom izrade zadace dosta mi je pomogao kod sa zadnjih vjezbi (gl_teksture), ali takodjer na internetu postoji mnogo izvora za OpenGL sto mi je olaksalo rad.
RAZNI IZVORI:
  Kreiranje kvadra:
	http://www.songho.ca/opengl/gl_vertexarray_quad.html
	http://www.wikihow.com/Make-a-Cube-in-OpenGL 
	http://www.ntu.edu.sg/home/ehchua/programming/opengl/CG_Examples.html

  Opcenito OpenGL:
	http://openglbook.com/the-book/chapter-1-getting-started/
	http://www3.ntu.edu.sg/home/ehchua/programming/opengl/CG_Introduction.html


  Rotacija:
	http://www.swiftless.com/tutorials/opengl/rotation.html
	http://pastebin.com/Sc2fgy4m
	http://www.youtube.com/watch?v=qKTIxw2RvkI

  Lightning:
	http://math.hws.edu/graphicsnotes/c4/s3.html
	http://www.glprogramming.com/red/chapter05.html