cmake_minimum_required(VERSION 2.6)
project(Zadaca)

set(DATA_FOLDER "${PROJECT_SOURCE_DIR}/data/")

INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR})

find_package(GLUT)
find_package(OpenGL)

cmake_minimum_required(VERSION 2.6)
INCLUDE_DIRECTORIES(${GLUT_INCLUDE_DIR} ${OPENGL_INCLUDE_DIR} ${PROJECT_SOURCE_DIR})

add_executable(nacinovic nacinovic.cpp)
target_link_libraries(nacinovic ${GLUT_LIBRARY} ${OPENGL_LIBRARY} ) 