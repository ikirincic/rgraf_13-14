#include <iostream>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


void display();
void specialKeys(int key, int x, int y);

double rotate_y=0;
double rotate_x=0;



int main(int argc, char* argv[]){


  glutInit(&argc,argv);


  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);


  glutCreateWindow("Kocka");


  glEnable(GL_DEPTH_TEST);


  glutDisplayFunc(display);
  glutSpecialFunc(specialKeys);

  glutMainLoop();

  return 0;
}

void display(){


  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);


  glLoadIdentity();


  glRotatef( rotate_x, 1.0, 0.0, 0.0 );
  glRotatef( rotate_y, 0.0, 1.0, 0.0 );



  //Prednja -  siva
  glBegin(GL_POLYGON);

  glColor3f(   0.5,  0.5, 0.5 );
  glVertex3f(  0.5, -0.5, -0.5 );
  glVertex3f(  0.5,  0.5, -0.5 );
  glVertex3f( -0.5,  0.5, -0.5 );
  glVertex3f( -0.5, -0.5, -0.5 );

  glEnd();

  // Straznja - bijela
  glBegin(GL_POLYGON);
  glColor3f(   1.0,  1.0, 1.0 );
  glVertex3f(  0.5, -0.5, 0.5 );
  glVertex3f(  0.5,  0.5, 0.5 );
  glVertex3f( -0.5,  0.5, 0.5 );
  glVertex3f( -0.5, -0.5, 0.5 );
  glEnd();

  // Desna - roza
  glBegin(GL_POLYGON);
  glColor3f(  1.0,  0.0,  1.0 );
  glVertex3f( 0.5, -0.5, -0.5 );
  glVertex3f( 0.5,  0.5, -0.5 );
  glVertex3f( 0.5,  0.5,  0.5 );
  glVertex3f( 0.5, -0.5,  0.5 );
  glEnd();

  // Lijeva -  zelena
  glBegin(GL_POLYGON);
  glColor3f(   0.0,  1.0,  0.0 );
  glVertex3f( -0.5, -0.5,  0.5 );
  glVertex3f( -0.5,  0.5,  0.5 );
  glVertex3f( -0.5,  0.5, -0.5 );
  glVertex3f( -0.5, -0.5, -0.5 );
  glEnd();

  // Gornja - plava
  glBegin(GL_POLYGON);
  glColor3f(   0.0,  0.0,  1.0 );
  glVertex3f(  0.5,  0.5,  0.5 );
  glVertex3f(  0.5,  0.5, -0.5 );
  glVertex3f( -0.5,  0.5, -0.5 );
  glVertex3f( -0.5,  0.5,  0.5 );
  glEnd();

  // Donja - crvena
  glBegin(GL_POLYGON);
  glColor3f(   1.0,  0.0,  0.0 );
  glVertex3f(  0.5, -0.5, -0.5 );
  glVertex3f(  0.5, -0.5,  0.5 );
  glVertex3f( -0.5, -0.5,  0.5 );
  glVertex3f( -0.5, -0.5, -0.5 );
  glEnd();

  glFlush();
  glutSwapBuffers();

}


void specialKeys( int key, int x, int y ) {


  if (key == GLUT_KEY_RIGHT)
    rotate_y -= 5;


  if (key == GLUT_KEY_LEFT)
    rotate_y += 5;

  if (key == GLUT_KEY_UP)
    rotate_x += 5;

  if (key == GLUT_KEY_DOWN)
    rotate_x -= 5;


  glutPostRedisplay();
}

