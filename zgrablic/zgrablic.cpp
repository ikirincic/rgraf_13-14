#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>

const int size=25;
int i=3;
float speed=0.005f;
float inc=0.0010f;



bool start=true;
bool turn=false;

void handleKeypress(unsigned char key, int x, int y){
    if (x && y) {}
    switch(key){
        case '+':
            speed+=inc;
            if(speed>0.1f){speed=0.1f;}
            printf("Speed:%.1f\n", speed*1000);
            break;
        case '-':
            speed-=inc;
            if(speed<0.0001f){speed=0.0000f;}
            printf("Speed:%.1f\n", speed*1000);
            break;
        case 'p':
            if(start){
                start=false;
                printf("Pause: ON\n");
            }else{
                start=true;
                printf("Pause: OFF\n");
            }
            break;
        case 'n':
            if(turn){
                turn=false;
                printf("Nazad: OFF\n");
            }else{
                turn=true;
                printf("Nazad: ON\n");
            }
            break;
        case 27:
        exit(0);
    }
}

void handleResize(int w, int h) {

    if(h == 0)
        h = 1;

    float ratio = 1.0* w / h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glViewport(0, 0, w, h);

    gluPerspective(45,ratio,1,1000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0,0.0,5.0,
              0.0,0.0,-1.0,
              0.0f,1.0f,0.0f);


}

float angle=0.0;

void drawScene(void) {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();

    glRotatef(angle,0.0,0.0,1.0);

    GLUquadricObj *newpolygon=gluNewQuadric( );

    gluSphere( newpolygon, 1.0f, i, i );


    glEnd();

    glPopMatrix();

    glutSwapBuffers();


    if(start){
        if(!turn){
            angle+=speed;
        }else{
            angle-=speed;
        }
    }
}


void update(int value){
    if (value){}
    if(start){
        if(!turn){
            i+=1;
        }else{
            i-=1;
        }
    }

    if(i > size){
        i=3;
    }else if(i<3){
        i=size;
    }
    glutPostRedisplay();
    glutTimerFunc(1000, update,0);
}


int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    printf("speed vrtnje:  |0-100|\n");
    printf("Ubrzaj:         |  +  |\n");
    printf("Uspori:         |  -  |\n");
    printf("Nazad(ON/OFF):  |  n  |\n");
    printf("Pauza(ON/OFF):	|  p  |\n");
    printf("Izlaz:          | Esc |\n");

    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

    glutInitWindowPosition(0,100);
    glutInitWindowSize(400,400);
    glutCreateWindow("David Zgrablic - animacija od poligona do kruga");

    glutIdleFunc(drawScene);
    glutKeyboardFunc(handleKeypress);

    glutReshapeFunc(handleResize);

    glEnable(GL_DEPTH_TEST);
    glutTimerFunc(1000,update,0);

    glutMainLoop();

    return 0;
}
